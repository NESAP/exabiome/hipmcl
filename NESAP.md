# Baseline benchmark problem

## Description

HipMCL: High Performance implementation of Markov Clustering Algorithm for the
clustering of large-scale protein-protein interaction networks.

References:
* https://academic.oup.com/nar/article/46/6/e33/4791133


## Figure of Merit
Time to solution

# Measurement on Edison
The measurement on Edison is done using 256 nodes.

The input network tested for this benchmark can be found at:
* https://portal.nersc.gov/project/m1982/HipMCL/isolates/iso_m100_subgraphs/subgraph1_iso_vs_iso_30_70length_ALL.m100.indexed.mtx
Note that this file already exists in NERSC filesystem and can be found under:
/project/projectdirs/m1982/HipMCL/iso_m100/subgraph1/subgraph1_iso_vs_iso_30_70length_ALL.m100.indexed.mtx
Hence, there is no need to explicitly store it.

```shell
edison$ make
edison$ mkdir data && cd data && ln -s /project/projectdirs/m1982/HipMCL/iso_m100/subgraph1/subgraph1_iso_vs_iso_30_70length_ALL.m100.indexed.mtx
edison$ sbatch run-edison.sh
edison$ grep "Total time:" hipmcl.o13393407
Total time: 2669.81
```

The time is in terms of seconds.


# Measurement on Cori Haswell

