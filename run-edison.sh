#!/bin/bash -l

#SBATCH -p regular
#SBATCH -N 256
#SBATCH -t 01:00:00
#SBATCH -J hipmcl
#SBATCH -o hipmcl.o%j
#SBATCH -A m1759

basedir=.
MAT=${basedir}/data/subgraph1_iso_vs_iso_30_70length_ALL.m100.indexed.mtx

export OMP_NUM_THREADS=24
N=256
n=256

srun -N $N -n $n -c 24  --ntasks-per-node=1 --cpu_bind=cores \
	${basedir}/mcl -M $MAT --matrix-market -I 2 -base 0 -per-process-mem 64

