COMBBLAS = .
BOOST = $(BOOST_DIR)
COMBBLAS_INC = $(COMBBLAS)/include/CombBLAS
COMBBLAS_SRC = $(COMBBLAS)/src
INCADD = -I$(COMBBLAS)/include/ -I$(COMBBLAS)/psort-1.0/include/ -I$(COMBBLAS)/usort/include/ -I$(BOOST)/include -I$(COMBBLAS)/graph500-1.2/generator/include/ 

GCCOPT = -O2 -DMPICH_IGNORE_CXX_SEEK -DGRAPH_GENERATOR_SEQ #-DNDEBUG (disables important assertions)
GCCDEB = -g -fno-inline -DMPICH_IGNORE_CXX_SEEK -DGRAPH_GENERATOR_SEQ #-DDEBUG
OPTPGI = -fast -Mipa=fast,inline -Msmartalloc --zc_eh -DMPICH_IGNORE_CXX_SEEK -DGRAPH_GENERATOR_SEQ 
COMPILER = CC 
GCCFLAGS = $(GCCOPT) #-DTIMING 
PGIFLAGS = $(INCADD) $(OPTPGI) -DCOMBBLAS_BOOST
CRAYFLAGS = $(INCADD) -DCOMBBLAS_BOOST -DCRAYCOMP -h msglevel_4
FLAGS = $(GCCFLAGS) -std=c++11 -std=gnu++14 -DTHREADED -fopenmp #-DCOMBBLAS_DEBUG #-DKSELECTLIMIT=40

default: mcl

#
# build Graph500 generator
#
$(COMBBLAS)/graph500-1.2/generator/libgraph_generator_seq.a:
	$(MAKE) -C $(COMBBLAS)/graph500-1.2/generator

CommGrid.o:	$(COMBBLAS_SRC)/CommGrid.cpp $(COMBBLAS_INC)/CommGrid.h
	$(COMPILER) $(INCADD) $(FLAGS) -c -o CommGrid.o $(COMBBLAS_SRC)/CommGrid.cpp 
mmio.o: $(COMBBLAS_SRC)/mmio.c
	cc $(INCADD) $(FLAGS) -c -o mmio.o $(COMBBLAS_SRC)/mmio.c

MPIType.o:	$(COMBBLAS_SRC)/MPIType.cpp $(COMBBLAS_INC)/MPIType.h
	$(COMPILER) $(INCADD) $(FLAGS) -c -o MPIType.o $(COMBBLAS_SRC)/MPIType.cpp 

MemoryPool.o:	$(COMBBLAS_SRC)/MemoryPool.cpp $(COMBBLAS_INC)/SpDefs.h
	$(COMPILER) $(INCADD) $(FLAGS) -c -o MemoryPool.o $(COMBBLAS_SRC)/MemoryPool.cpp 

hash.o:	$(COMBBLAS_SRC)/hash.cpp $(COMBBLAS_INC)/hash.hpp
	$(COMPILER) $(FLAGS) $(INCADD) -c -o hash.o $(COMBBLAS_SRC)/hash.cpp


base/MCL.o:  base/MCL.cpp base/CC.h base/WriteMCLClusters.h $(COMBBLAS_INC)/SpDCCols.cpp $(COMBBLAS_INC)/dcsc.cpp $(COMBBLAS_INC)/SpHelper.h $(COMBBLAS_INC)/SpParMat.h $(COMBBLAS_INC)/ParFriends.h $(COMBBLAS_INC)/SpParMat.cpp $(COMBBLAS_INC)/SpDefs.h $(COMBBLAS_INC)/SpTuples.cpp
	$(COMPILER) $(INCADD) $(FLAGS) -c -o base/MCL.o base/MCL.cpp 

mcl:	MemoryPool.o CommGrid.o MPIType.o base/MCL.o mmio.o hash.o
	$(COMPILER) $(INCADD) $(FLAGS)  -o mcl base/MCL.o MemoryPool.o mmio.o CommGrid.o MPIType.o hash.o

clean:
	rm -f mcl
	rm -f *.o
	rm -f $(COMBBLAS)/graph500-1.2/generator/*.o
	rm -f $(COMBBLAS)/graph500-1.2/generator/libgraph_generator_seq.a

